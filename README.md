# Wordpress back-end for Karlssonwilker game

Live
----
http://nelsonvassalo.com/kw/wp/

## — WP admin Login same as for v-a


Instructions
----

- Develop on `develop` branch
- To deploy, merge `develop` onto `master`
- Deployment is triggered automatically. Check for __successful__ build.
- Once deployed, rebranch again to `develop`

Database
----
**Needs to be reuploaded manually**
