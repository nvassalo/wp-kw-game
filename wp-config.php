<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kw-game');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6@Q^{Iz=CJo&v;24/EX+``Gu=Nc6T/=L_(I?Xz?<+nW9:OQHUY}J[Qsnb=KQb{Sz');
define('SECURE_AUTH_KEY',  's[l_Snk<^#]]nUoa5}c<AS= 7p7T4W`kr#TM*;}I0XoucTcFd<P;#>^kUHm%:C@~');
define('LOGGED_IN_KEY',    '%KbF/6{C?C4&v,z0%`#mR,`,M[2Dgb@hYD;qp~ -shS6(,=+=(kc=?bZC1vJ{E|;');
define('NONCE_KEY',        'qnb!cY,TPe2)r-{3.%Z4gdf_DwV|f=31|<%<wjA`:^^)Kd=lY~]AW]Cm wNATzUK');
define('AUTH_SALT',        'yTN;j3A?1|:j3p$f*`UK*Ss1Hvyp;O67r8JG_f}f4UP1]U;m`Tj(6_[o^^s+:dKt');
define('SECURE_AUTH_SALT', 'bZ!b4O LnO}:ul7i+l$c[iXRmhBpzMKxG4gxC 5B?Kie?<gup^<9(l]*FkbM;mb4');
define('LOGGED_IN_SALT',   'w4R9O&OF:R=<dd[/;b+4Q)Q>(Na=HNlYP18[H?*HcHCEb!`E9.b`[[rzY*PUqiLd');
define('NONCE_SALT',       'K!wI0$N;nDHpYpESus-G~,MW-w6jKlszS6O>j7!EjVhOJoI[hat.c9Nao~SeF#Pr');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
