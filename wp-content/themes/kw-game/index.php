<?php

	$args = new WP_Query(array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'order'   => 'ASC',
	));

	$query = new WP_Query($args);

	if ($query->have_posts()) :

		while ($query->have_posts()) :

			$query->the_post();

			$designers = get_field('designers');
			$boss = get_field('boss');
			$boss_content = get_field('boss_content');

			// To use if json gets too big
			// $i=0;
			// foreach ($designers as $designer) {
			// 	unset($designers[$i]['featured_image']['ID']);
			//	etc…
			// }
			// $i++;


			$query_data[] = array(
					'title' => get_the_title(),
					'designers' => $designers,
					'boss' => $boss,
					'boss_content' => $boss_content
			);

		endwhile;

		wp_reset_postdata();
	endif;

	echo wp_send_json($query_data);

?>


<?php get_footer();
